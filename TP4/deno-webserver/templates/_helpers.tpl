{{/*Expand the name of the chart.*/}}
{{- define "deno-webserver.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "deno-webserver.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/* Fullname suffixed with weberserver */}}
{{- define "deno-webserver.webserver.fullname" -}}
{{- printf "%s-webserver" (include "deno-webserver.fullname" .) -}}
{{- end }}

{{/* Fullname suffixed with mariadb */}}
{{- define "deno-webserver.mariadb.fullname" -}}
{{- printf "%s-mariadb" (include "deno-webserver.fullname" .) -}}
{{- end }}

{{/* Create chart name and version as used by the chart label. */}}
{{- define "deno-webserver.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/* Create chart name and version as used by the chart label. */}}
{{- define "deno-webserver.chartref" -}}
{{- replace "+" "_" .Chart.Version | printf "%s-%s" .Chart.Name -}}
{{- end }}

{{/* Common labels */}}
{{- define "deno-webserver.labels" -}}
helm.sh/chart: {{ include "deno-webserver.chart" . }}
app.kubernetes.io/version: "{{ replace "+" "_" .Chart.Version }}"
app.kubernetes.io/managed-by: {{ .Release.Service }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ template "deno-webserver.name" . }}
chart: {{ template "deno-webserver.chartref" . }}
release: {{ $.Release.Name | quote }}
heritage: {{ $.Release.Service | quote }}
{{- if .Values.commonLabels}}
{{ toYaml .Values.commonLabels }}
{{- end -}}
{{- end -}}

{{/* Create the name of the webserver service account to use */}}
{{- define "deno-webserver.webserver.serviceAccountName" -}}
{{- if .Values.webserver.serviceAccount.create }}
{{- default (include "deno-webserver.fullname" .) .Values.webserver.serviceAccount.name }}-webserver
{{- else }}
{{- default "default" .Values.webserver.serviceAccount.name }}
{{- end }}
{{- end }}
{{/* Create the name of the mariadb service account to use */}}
{{- define "deno-webserver.mariadb.serviceAccountName" -}}
{{- if .Values.mariadb.serviceAccount.create }}
{{- default (include "deno-webserver.fullname" .) .Values.mariadb.serviceAccount.name }}-mariadb
{{- else }}
{{- default "default" .Values.mariadb.serviceAccount.name }}
{{- end }}
{{- end }}

{{/* Fix error in ingress naming */}}
{{- define  "deno-webserver.webserver.ingressName" -}}
{{- if .Values.webserver.service.name }}{{.Values.webserver.service.name}}{{ else }}{{ template "deno-webserver.webserver.fullname" . }}{{ end }}
{{- end -}}