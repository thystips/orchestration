= TP4-5-6

== Chart HELM

L'accès à au chart HELM utilisé pour les TP se fait via le `Package Registry` du dépôt, il est possible d'y accéder via l'interface ou en ajoutant le dépôt dans HELM.

https://gitlab.com/thystips/orchestration/-/packages[Package Registry]

.Ajout du dépôt dans helm
[source,bash]
----
$ helm repo add orchestration https://gitlab.com/api/v4/projects/34198605/packages/helm/stable
$ helm repo update
----

Pour l'utilisation de ce chart je recommande de modifier les variables par défaut.

.Récupération des variables
[source,bash]
----
$ helm show values orchestration/deno-webserver > deno-webserver.yaml
----

Vous pouvez maintenant éditer le fichier `deno-webserver.yaml` en fonction de vos besoins.

TIP: Pour activer l'ingress vous devez éditer la section relative à l'ingress du webserver.

.Installation du chart `deno-webserver`
[source,bash]
----
$ helm install deno orchestration/deno-webserver -f ./deno-webserver.yaml
----

== Debug

En cas de problème vous pouvez afficher les fichiers générés par helm.

WARNING: Cette commence utilise `batcat` pour l'affichage, vous pouvez également utilisé `cat`. L'affichage avec `batcat` étant plus lisible vous trouverez la documentation https://github.com/sharkdp/bat#installation[ici].

.Debug
[source,bash]
----
$ helm install deno orchestration/deno-webserver -f ./values.yaml --dry-run --debug | batcat - -l Yaml
----

